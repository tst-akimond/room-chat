import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, View, ImageBackground, Dimensions, StatusBar } from 'react-native'
import styles from './Styles/LaunchScreenStyles'
import Swiper from './Swiper/index';
import ThreeDot from './ThreeDot';
import AuthForm from './authForm';
export default class LaunchScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      curindex: 0,
    }
  }
  render() {
    let { curindex } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <View
          style={styles.swipcontainer}>
          <Swiper
            index={0}
            autoplay={true}
            loop={true}
            dotColor="#bbbbbb"
            activeDotColor="#ffffff"
            showsButtons={false}
            showsPagination={false}
            autoplayTimeout={3}
            onIndexChanged={(index) => this.setState({ curindex: index })}
            style={styles.wrapper}>
            <ImageBackground
              style={styles.swipeitem}
              source={require('../Images/s1.png')}
            />
            <ImageBackground
              style={styles.swipeitem}
              source={require('../Images/s2.png')}
            />
            <ImageBackground
              style={styles.swipeitem}
              source={require('../Images/s3.png')}
            />
          </Swiper>
        </View>
        <View style={styles.titlecontainer}>
          <View style={styles.titlecontent}>
            <Text style={styles.title}>
              Welcome
            </Text>
            <Text
              style={styles.text}>
              Sign Up or Sign In
            </Text>
            <ThreeDot index={curindex} />
          </View>
          <View style={styles.bottomcontainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Signup')}
              style={styles.button}>
              <Image style={styles.image}
                source={require('../Images/s1.png')}
              />
              <Text style={styles.buttontext}>
                Get started
              </Text>
              <Image style={styles.icon}
                source={require('../Images/ic_right.png')} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Signin')}
              style={styles.button1}>
              <Image style={styles.image}
                source={require('../Images/s1.png')}
              />
              <Text style={styles.buttontext}>
                Login
              </Text>
              <Image style={styles.icon}
                source={require('../Images/ic_right.png')} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
console.disableYellowBox = true;