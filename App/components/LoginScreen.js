import React, { Component } from 'react';
import { Text, View, Button, StyleSheet, TextInput, ActivityIndicator, TouchableOpacity, Image } from 'react-native';
import Validation from './valdation';
import auth from '@react-native-firebase/auth';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import database from '@react-native-firebase/database';
import { GoogleSignin } from '@react-native-community/google-signin';
import styles from './Styles/LoginScreenStyles'

GoogleSignin.configure({
  webClientId: '102666776687-20s39e372ohebluvr74pgh1vd1plvcnb.apps.googleusercontent.com',
});

class LoginScreen extends Component {
  facebookCredential = ""
  state = {
    loading: false,
    type: 'Login',
    action: 'Login',
    actionMode: 'Don\'t have account? Signup',
    form: {
      username: {
        value: "",
        valid: false,
        rules: {
          'isRequired': true,
        }
      },
      email: {
        value: "",
        valid: false,
        rules: {
          'isRequired': true,
          'isEmail': true
        }
      },
      password: {
        value: "",
        valid: false,
        rules: {
          'isRequired': true,
          'minLength': 6,
        }
      },
      confirm_password: {
        value: "",
        rules: {
          'confirmPass': "password"
        }
      },
    },
    hasErrors: false,
    users: []
  }
  /*
  async onFacebookButtonPress()
  {
      alert()
      const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
      if (result.isCancelled) {
          throw 'User cancelled the login process';
      }

      const data = await AccessToken.getCurrentAccessToken();
      if (!data) {
          throw 'Something went wrong obtaining access token';
        }
      
        console.log("facebook data is");
        console.log(data)
        const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);


  }
  */

  async  onFacebookButtonPress() {
    // Attempt login with permissions
    try {
      this.setState({
        loading: true
      });
      /*  
          this.setState({
              loading:true,
              hasErrors:false
          })*/
      const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

      if (result.isCancelled) {
        throw 'User cancelled the login process';
      }

      // Once signed in, get the users AccesToken
      const data = await AccessToken.getCurrentAccessToken();


      if (!data) {
        throw 'Something went wrong obtaining access token';
      }

      console.log("here")
      // Create a Firebase credential with the AccessToken    
      const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

      let userFbdata = await auth().signInWithCredential(facebookCredential);

      let fireBasedata = await database()
        .ref('/users/').orderByChild("username").equalTo(auth().currentUser.email)
        .once('value').then(snapshot => {
          return snapshot.val()
        })

      if (fireBasedata == null) {
        let account = {}

        account.email = auth().currentUser.email;
        account.uid = auth().currentUser.uid;
        account.username = auth().currentUser.email;
        account.display_name = auth().currentUser.displayName;


        database().ref("/users/" + auth().currentUser.uid).set(
          account
        ).then(() => {
          this.setState({
            loading: false
          });

        })

      }




      // Sign-in the user with the credential
      //  return auth().signInWithCredential(facebookCredential);
    }

    catch (error) {
      console.log(error)
      this.setState({
        hasErrors: true
      })

    }
  }

  async  onGoogleButtonPress() {
    // Get the users ID token
    try {
      this.setState({
        loading: true
      })
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      const googleCredential = await auth.GoogleAuthProvider.credential(userInfo.idToken);
      await auth().signInWithCredential(googleCredential);
      let data = await database()
        .ref('/users/').orderByChild("username").equalTo(userInfo.user.email)
        .once('value').then(snapshot => {
          return snapshot.val()
        })
      if (data == null) {
        let account = {}

        account.email = auth().currentUser.email;

        account.uid = auth().currentUser.uid;
        account.username = auth().currentUser.email;
        account.display_name = auth().currentUser.displayName;


        database().ref("/users/" + auth().currentUser.uid).set(
          account
        ).then(() => {
          this.setState({
            loading: false
          });

        })

      }
      this.setState({
        hasErrors: false
      })

    }
    catch (error) {

      this.setState({
        hasErrors: true
      })
    }
  }




  updateInput = (name, val) => {
    this.setState({
      hasErrors: false
    })
    let formcopy = this.state.form
    formcopy[name].value = val
    let rules = formcopy[name].rules
    let valid = Validation(val, rules, formcopy)
    console.log(valid)
    formcopy[name].valid = valid

    this.setState({
      form: formcopy
    })
  }

  changeFormType = () => {

    const type = this.state.type

    this.setState({
      type: type === "Login" ? "Register" : "Login",
      action: type === "Login" ? "Register" : "Login",
      actionMode: type === "Login" ? "Already have account? Login" : "Don't have Account? Signup",

    })

  }

  userName = () => {

    return this.state.type != "Login" ?
      <TextInput style={styles.input}
        value={this.state.form.username.value}
        placeholder={"User Name"}
        onChangeText={(value => this.updateInput("username", value))}
      />
      : null
  }
  confirmPassword = () => {
    return this.state.type != "Login" ?
      <TextInput style={styles.input}
        value={this.state.form.confirm_password.value}
        placeholder={"Confirm Password"}
        onChangeText={(value => this.updateInput("confirm_password", value))}
        secureTextEntry
      />
      : null;


  }

  socailLogins() {
    return (
      this.state.type === "Login" ?
        <>
          <View>
            <TouchableOpacity
              onPress={() => this.onGoogleButtonPress().then(() => {
                console.log('Signed in with Google!');
              })}
              style={styles.button}>
              <Image style={styles.image}
                source={require('../Images/ic_google.png')} />
              <Text style={styles.buttontext}>
                Sign in with Google
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.onFacebookButtonPress().then(() => {
                console.log('Signed in with Facebook!');
              })}
              style={styles.button1}>
              <Image style={styles.image}
                source={require('../Images/ic_facebook.png')} />
              <Text style={styles.buttontext}>
                Sign in with Facebook
              </Text>
            </TouchableOpacity>
          </View>
        </> : null
    );
  }

  formhasErrors = () => (

    this.state.hasErrors ?
      <View style={styles.errorContainer}>
        <Text style={styles.errorLabel}>Oops, check your info</Text>
      </View>
      : null

  )

  submitUser = () => {
    let isFormValid = true;
    let formtoSubmit = {}
    let formCopy = this.state.form;
    for (let key in formCopy) {
      if (this.state.type === "Login") {
        if (key !== "confirm_password" && key !== "username") {
          isFormValid = isFormValid && formCopy[key].valid
          formtoSubmit[key] = formCopy[key].value
        }
      }
      else {
        isFormValid = isFormValid && formCopy[key].valid
        formtoSubmit[key] = formCopy[key].value
      }
    }
    if (isFormValid) {
      this.setState({
        loading: true
      })
      if (this.state.type == "Login") {
        auth()
          .signInWithEmailAndPassword(formtoSubmit.email, formtoSubmit.password)
          .then(() => {
            this.setState({
              loading: false
            })
          })
          .catch(error => {
            console.log(error)
            this.setState({
              loading: false
            })
            this.setState({
              hasErrors: true
            })
          });
      }
      else {
        database()
          .ref('/users/').orderByChild("username").equalTo(formtoSubmit.username)
          .once('value', snapshot => {
            var userData = snapshot.val();
            if (userData === null) {
              auth()
                .createUserWithEmailAndPassword(formtoSubmit.email, formtoSubmit.password)
                .then((authData) => {
                  let account = {}
                  console.log(authData)
                  account.email = formtoSubmit.email,

                    account.uid = authData.user.uid,
                    account.username = formtoSubmit.username
                  database().ref("/users/" + authData.user.uid).set(
                    account
                  ).then(() => {
                    this.setState({
                      loading: false
                    })

                  })



                })
                .catch(error => {
                  console.log(error.code)
                  /*
                  if (error.code === 'auth/email-already-in-use') {
                  console.log('That email address is already in use!');
                  }
 
                  if (error.code === 'auth/invalid-email') {
                  console.log('That email address is invalid!');
                  }*/

                  this.setState({
                    hasErrors: true
                  })

                });
            }
            else {
              this.setState({
                hasErrors: true
              })

            }
          });

      }

    }
    else {
      this.setState({
        hasErrors: true
      })
    }

  }

  manageAccess = () => {
    if (this.props.User.auth.uid) {
      setTokens(this.props.User.auth, () => {
        this.setState({
          hasErrors: false
        })
        //got to next screen
        this.props.goNext();

      })
    }
    else {
      this.setState({
        hasErrors: true
      })
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator />
        </View>
      )
    }
    else {
      return (
        <View style={styles.container}>
          <View style={styles.titlecontent}>
            <Text style={styles.title}>
              Sign In
            </Text>
          </View>
          {this.socailLogins()}
          {this.userName()}
          <View style={{height:20,}}></View>
          <View style={styles.inputcontainer}>
            <TextInput style={styles.input}
              value={this.state.form.email.value}
              keyboardType={"email-address"}
              placeholder={"Email Address"}
              onChangeText={(value => this.updateInput("email", value))}
            />
          </View>
          <View style={styles.inputcontainer}>
            <TextInput style={styles.input}
              value={this.state.form.password.value}
              placeholder={"Password"}
              onChangeText={(value => this.updateInput("password", value))}
              secureTextEntry
            />
          </View>
          {this.confirmPassword()}
          {this.formhasErrors()}
          <TouchableOpacity
            onPress={this.submitUser}
            style={styles.button2}>
            <Text style={[styles.buttontext, { color: 'white' }]}>
              Log In
            </Text>
          </TouchableOpacity>
          <Text style={styles.buttontext4}>
            You are completely safe.
              </Text>
          <Text style={styles.buttontext5}>
            Read our Terms & Conditions.
              </Text>
        </View>
      )
    }
  }
}
export default LoginScreen



