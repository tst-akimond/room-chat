import React from 'react';
import { StyleSheet, ScrollView, View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import auth from '@react-native-firebase/auth';
import AuthForm from './authForm';
import LaunchScreen from './modules/auth/LaunchScreen';
import LoginScreen from './LoginScreen';
import SignupScreen from './SignupScreen';
import Create from './create';
import Home from './drawerNavigation';
import PreChat from './preChat';
import Chat from './chat';
import Groupoptions from './groupOptions'
import messaging from '@react-native-firebase/messaging';
import database from '@react-native-firebase/database';
import { createDrawerNavigator } from '@react-navigation/drawer';

const AuthStack = createStackNavigator();
const AppStack = createStackNavigator();

const headeroptions = ({ navigation }) => {

    return {
        headerTitleAlign: 'center',
        headerMode: 'screen',

        headerRight: () => (
            <Button
                onPress={() => navigation.navigate('Create')}
                title="Crete Group"
                color="#00f"
            />
        ),

    }
}

export default class Navs extends React.Component {

    state = {
        isSign: false
    }

    changeIsSignState = () => {
        this.setState({
            isSign: true
        })
    }

    async setDeviceTokenUser(token) {
        let responese = await database().ref("/users/" + auth().currentUser.uid).update({
            deviceToken: token
        }).then(res => {
            return res
        })
    }
    componentDidMount() {
        auth().onAuthStateChanged(user => {
            if (user) {
                messaging()
                    .getToken()
                    .then(token => {
                        this.setDeviceTokenUser(token)
                        this.setState({
                            isSign: true
                        })
                    });
                messaging().onTokenRefresh(token => {
                    this.setDeviceTokenUser(token)
                    this.setState({
                        isSign: true
                    })
                });
            }
            else {
                this.setState({
                    isSign: false
                })
            }

        });
    }
    /*
        componentWillUnmount(){
            this.unsubscribe ();
        }
    */
    render() {
        return (
            !this.state.isSign ?
                <NavigationContainer>
                    <AuthStack.Navigator
                        screenOptions={{
                            headerShown: false
                        }}
                        initialRouteName="LaunchScreen">
                        <AuthStack.Screen name="LaunchScreen" component={LaunchScreen} />
                        <AuthStack.Screen name="Signup" component={SignupScreen} />
                        <AuthStack.Screen name="Signin" component={LoginScreen} />
                    </AuthStack.Navigator>
                </NavigationContainer>
                :
                <NavigationContainer>
                    <AppStack.Navigator
                        // screenOptions={{
                        //     headerShown: false
                        // }}
                    >
                        <AppStack.Screen name="Home" component={Home}
                            options={({ navigation }) => {
                                return {
                                    headerTitleAlign: 'center',
                                    headerMode: 'screen',

                                    headerRight: () => (
                                        <Button
                                            onPress={() => navigation.navigate('Options')}
                                            title="Crete Group"
                                            color="#00f"
                                        />
                                    ),

                                }
                            }}

                        />
                        <AppStack.Screen name="Options" component={Groupoptions} />
                        <AppStack.Screen name="Create" component={Create} />
                        <AppStack.Screen name="Prechat" component={PreChat} />
                        <AppStack.Screen options={{headerShown: false}} name="Chat" component={Chat}
                            // options={({ navigation }) => {

                            //     return {
                            //         headerTitleAlign: 'center',
                            //         headerMode: null,

                            //         headerLeft: () => (
                            //             <Button
                            //                 onPress={() => navigation.navigate('Home')}
                            //                 title="Home"
                            //                 color="#00f"
                            //             />
                            //         ),

                            //     }
                            // }}
                        />
                    </AppStack.Navigator>
                </NavigationContainer>


        )
    }
}


