import React, {Component} from 'react';
import {StyleSheet,View,Text,TextInput,Button,ActivityIndicator} from 'react-native';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import CheckBox from '@react-native-community/checkbox';
import Validation from './valdation';


class CreateComponent extends Component{
    
    state={
        loading:false,
        hasErrors:false,
        checked:false,
        username:"",
        form:{
            group:{
                value:"",
                valid:false,
                rules:{
                    'isRequired':true
                }
            },
            notes:{
                value:""
            }
        }        
    }

   

    


    updateInput = (name,val) => 
    {
        this.setState({
            hasErrors:false
        })
        let formcopy=this.state.form
        formcopy[name].value=val
        let rules=formcopy[name].rules
        let valid=Validation(val,rules,formcopy)
        console.log(valid)
        formcopy[name].valid=valid

        this.setState({
            form:formcopy
        })
    }

    formhasErrors=()=>(

        this.state.hasErrors?
            <View style={styles.errorContainer}>
                <Text style={styles.errorLabel}>Oops, check your info</Text>
            </View>
        :null
    )

    componentDidMount()
    {
        database()
        .ref('/users/'+auth().currentUser.uid)
        .once('value', snapshot => {
            let uData=snapshot.val();
           this.setState({
               username:uData.username
           })
        })
  
  
    }

    submitForm=()=>{
        let isFormValid=this.state.form
        let formtoSubmit={}
        let formCopy=this.state.form;
        
        for(let key in formCopy)
        {
            isFormValid=isFormValid && formCopy[key].valid
            formtoSubmit[key]=formCopy[key].value
        }

        if(isFormValid)
        {
            this.setState({
                loading:true
            })
           
            formtoSubmit['created_by']=auth().currentUser.email;
            formtoSubmit['created_id']=auth().currentUser.uid;
            formtoSubmit['notes']=this.state.notes;
            formtoSubmit['private']=false;
            formtoSubmit['mode']="storymode";
            formtoSubmit['status']="created";
           const ref= database()
                .ref('/groups')
                .push(formtoSubmit);
                
            
         
            if(ref.key)
            {
                database()
                .ref('/groups/'+ref.key+"/paritications").push({username:this.state.username,'uid':auth().currentUser.uid}).then(()=>{
                this.setState({
                    loading:false
                })  
                this.props.navigation.navigate("Prechat",{group_id:ref.key})
            })
            }
            else
            {
                this.setState({
                    loading:false
                })  
            }              

            
        }
        else
        {
            this.setState({
                hasErrors:true
            })
        }

    }

    render()
    {
        if(this.state.loading)
        {   return(
            <View style={styles.loading}>
            <ActivityIndicator/>
           </View>
            )
        }
        else
        {

        return (

                    <View style={styles.sectionContainer}>

                        <TextInput  style={styles.input}
                            placeholder={"Group name"}
                            value={this.state.form.group.value}
                            onChangeText={(value=>this.updateInput("group",value))}
                        />
                        {/* <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Text>Private</Text>
                        <CheckBox
                          value={this.state.checked}
                        onValueChange={()=>this.setState({checked:!this.state.checked})}
                        />
                        </View> */}
                        <TextInput  style={styles.input}
                            placeholder={"Notes"}
                            value={this.state.form.notes.value}
                            multiline={true}
                            onChangeText={(value=>this.updateInput("notes",value))}
                        />
                         {this.formhasErrors()}
                        <Button title="Create Group" style={styles.button}
                        onPress={this.submitForm}
                        />

                     
                    </View>
                    
        );
        }
    }
};

const styles = StyleSheet.create({
  
  sectionContainer: {
    flex:1,
    backgroundColor:'#fff',
    padding:50
  },
  input:{
    width:'100%',
    borderBottomWidth:3,
    borderBottomColor:'#eaeaea',
    fontSize:16,
    padding:5,
    marginTop:0,
    marginBottom:10,
},
button:{
    marginTop:20,
    marginBottom:10

},
loading:{
    flex:1,
    backgroundColor:'#fff',
    alignItems:"center",
    justifyContent:"center"
  },
  errorContainer:{
    marginTop:30,
    marginBottom:10,
    padding:10,
    backgroundColor:'#f44336'
},
errorLabel:{
    color:'#fff',
    textAlign:"center",
    textAlignVertical:"center"
    
}
  
});



export default CreateComponent;
