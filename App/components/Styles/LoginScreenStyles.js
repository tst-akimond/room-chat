import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
    flex: 1,
    alignItems: 'center',
  },
  swipcontainer: {
    height: Dimensions.get('window').height * 0.4,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  swipeitem: {
    width: '100%',
    height: '100%'
  },
  centered: {
    alignItems: 'center'
  },
  act_dot: {
    width: 9,
    height: 9,
    borderRadius: 9 / 2,
    margin: 3,
    backgroundColor: 'rgb(241, 98, 0)'
  },
  inact_dot: {
    width: 8,
    height: 8,
    borderRadius: 8 / 2,
    margin: 3,
    backgroundColor: 'rgb(252, 231, 215)'
  },
  titlecontainer: {
    height: Dimensions.get('window').height * 0.6,
  },
  titlecontent: {
    width: Dimensions.get('window').width * 0.9,
    alignSelf: 'center',
  },
  title: {
    fontSize: 30,
    fontFamily: 'MarketSans-Medium',
    marginTop: 100,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 20,
    fontFamily: 'MarketSans-Bold',
    marginTop: 10,
  },
  slidershow: {
    flexDirection: 'row',
    marginTop: 40,
    alignItems: 'center',
  },
  bottomcontainer: {
    width: '100%',
    height: Dimensions.get('window').height * 0.4,
    marginTop: 25,
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 25,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 20,
  },
  image: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 25,
  },
  buttontext: {
    marginLeft: 10,
    fontFamily: 'MarketSans-Bold',
    fontSize: 15,
    width: 200,
    textAlign: 'center',
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    marginLeft: 30,
    tintColor: 'black'
  },
  button1: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 25,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 25,
  },
  buttontext2: {
    fontFamily: 'MarketSans-Bold',
    fontSize: 12,
    textAlign: 'center',
    color: 'gray',
    marginTop: 35,
  },
  inputcontainer: {
    backgroundColor: 'white',
    height: 45,
    justifyContent: 'center',
    borderRadius: 45 / 2,
    width: '80%',
    paddingLeft: 45 / 2,
    marginTop: 10,
  },
  input: {
    width: '100%',
    fontSize: 15,
    padding: 5,
  },
  button2: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgb(112, 67, 242)',
    elevation: 5,
    borderRadius: 25,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 25,
    justifyContent: 'center',
  },
  buttontext4: {
    fontFamily: 'MarketSans-Bold',
    fontSize: 13,
    textAlign: 'center',
    color: 'black',
    marginTop: 50,
  },
  buttontext5: {
    fontFamily: 'MarketSans-Bold',
    fontSize: 13,
    textAlign: 'center',
    color: 'rgb(112, 67, 242)',
  },
  errorContainer: {
    marginTop: 30,
    marginBottom: 10,
    padding: 10,
    backgroundColor: '#f44336'
  },
  errorLabel: {
    color: '#fff',
    textAlign: "center",
    textAlignVertical: "center"
  },
  loading: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: "center",
    justifyContent: "center"
  },
  errorContainer: {
    marginTop: 20,
    padding: 10,
    backgroundColor: '#f44336',
    borderRadius: 20,
  },
  errorLabel: {
    color: '#fff',
    textAlign: "center",
    textAlignVertical: "center"
  },
  loading: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: "center",
    justifyContent: "center"
  },
  button3: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 25,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 20,
  },
})
