import { StyleSheet,Dimensions } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  swipcontainer: {
    height: Dimensions.get('window').height * 0.4,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  swipeitem: {
    width: '100%',
    height: '100%'
  },
  centered: {
    alignItems: 'center'
  },
  act_dot: {
    width: 9,
    height: 9,
    borderRadius: 9 / 2,
    margin: 3,
    backgroundColor: 'rgb(241, 98, 0)'
  },
  inact_dot: {
    width: 8,
    height: 8,
    borderRadius: 8 / 2,
    margin: 3,
    backgroundColor: 'rgb(252, 231, 215)'
  },
  titlecontainer: {
    height: Dimensions.get('window').height * 0.6,
  },
  titlecontent: {
    width: Dimensions.get('window').width * 0.9,
    alignSelf: 'center',
  },
  title: {
    fontSize: 12,
    fontFamily: 'MarketSans-Medium',
    marginTop: 25,
  },
  text: {
    fontSize: 20,
    fontFamily: 'MarketSans-Bold',
    marginTop: 10,
  },
  slidershow: {
    flexDirection: 'row',
    marginTop: 40,
    alignItems: 'center',
  },
  bottomcontainer: {
    backgroundColor: 'rgb(179, 209, 213)',
    width: '100%',
    height: Dimensions.get('window').height * 0.4,
    marginTop: 25,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 10,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 60,
  },
  image: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 70,
    opacity:0,
  },
  buttontext: {
    marginLeft: 10,
    fontFamily: 'MarketSans-Bold',
    fontSize: 15,
    width:100,
    textAlign:'center',
  },
  icon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    marginLeft: 30,
    tintColor: 'black'
  },
  button1: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 10,
    width: Dimensions.get('window').width * 0.8,
    height: 50,
    marginTop: 25,
  }
})
