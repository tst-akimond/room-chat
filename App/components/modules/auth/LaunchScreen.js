import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, View, ImageBackground, Dimensions, StatusBar, StyleSheet } from 'react-native'
import { Values, Colors, Images, Strings } from '../../../config'
import { IconizedButton } from '../../atoms'
import { WelcomeTutorial } from '../../molecules'
export default class LaunchScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      curindex: 0,
    }
  }

  onGetStart = () => {
    this.props.navigation.navigate('Signup')
  }

  onLogin = () => {
    this.props.navigation.navigate('Signin')
  }

  renderBackground = () => {
      return <View style={styles.backgroundContainer}>
          <Image style={styles.backgroundImage} source={Images.initialBg}/>
          <View style={styles.overlay}/>
      </View>
  }
  
  render() {
    return (
      <View style={styles.mainContainer}>
          {this.renderBackground()}
          <View style={{alignSelf: 'stretch', marginTop:130, alignItems: 'center'}}>
            <Image style={styles.logo} source={Images.logo}></Image>
          </View>

          <View style={{flex: 1}}>
              <WelcomeTutorial style={{width:300, height: 200, backgroundColor:'red'}}/>
          </View>
          <View style={styles.bottomButtonContainer}>
            <IconizedButton titleStyle={styles.buttonText} borderRadius={12} 
                            style={{flex:1, height:58, backgroundColor: Colors.primaryColor }} 
                            buttonTitle={Strings.txtGetStart} onPress={()=>{this.onGetStart()}}/>
            <IconizedButton titleStyle={styles.buttonText} borderRadius={12} 
                    style={{flex:1, height:58, backgroundColor: Colors.secondaryColor, marginTop: 18 }} 
                    buttonTitle={Strings.txtLogin} onPress={()=>{this.onLogin()}}/>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    mainContainer: {
        flexDirection: "column",
        backgroundColor: 'white',
        flex: 1,
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        resizeMode: 'cover'
    },
    logo: {
        width: 145,
        height: 40,
    },
    overlay: {
        flex: 1,
        backgroundColor: Colors.overlay
    },

    bottomButtonContainer: {
        height: 170,
        paddingVertical: 18,
        paddingHorizontal: 28,
        alignSelf: 'stretch'
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    }
   
});
console.disableYellowBox = true;