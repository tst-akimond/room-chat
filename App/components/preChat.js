import React, {Component} from 'react';
import {StyleSheet,View,Text,TextInput,Button,ActivityIndicator,ScrollView} from 'react-native';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import {Picker} from '@react-native-community/picker';
import Validation from './valdation'
import {sendPushNotification} from './notification';
import 'moment-timezone';
import moment from 'moment';

class PreChatComponent extends Component{
    
    state={
        loading:false,
        hasErrors:false,
        username:"",
        groupData:null,
        joined_users:"",
        deviceTokens:[]

        }        
    
    

    componentDidMount()
    {
        console.log("grp id here");
        console.log(this.props.route.params.group_id)
        
        database()
        .ref('/users/'+auth().currentUser.uid)
        .once('value', snapshot => {
            let uData=snapshot.val();
           this.setState({
               username:uData.username
           })
        })
        
        this.group=database()
            .ref('/groups/'+this.props.route.params.group_id)
            .on('value', snapshot => {
                if(!snapshot)
                    return

                let groupData=snapshot.val();
                console.log("grpData")
                console.log(groupData);
               
                if(groupData.status!="created")
                {
                   
                    this.props.navigation.navigate("Chat",{group_id:this.props.route.params.group_id})
                }
                else
                {
                    let joined_users=[];
                    let paritications=groupData.paritications;
                    for(let i in paritications)
                    {
                        joined_users.push(paritications[i].username)
                    }

                    this.setState({
                        groupData:groupData,
                        joined_users:joined_users
                    })
                    
        
                }   
            }); 
        

    }

    componentWillUnmount()
    {
        this.group();
       // this.user();

    }
    
    

    

    joinGroupFromId(groupid)
    {
        this.setState({
            loading:true
        });

        const newRef= database()
                .ref('/groups/'+groupid+'/paritications/')
                .push({
                    uid:auth().currentUser.uid,
                    username:this.state.username
                }).then(()=>{
                   this.setState({
                       loading:false
                   })
            })
    }

    joinOpenGroup(item)
    {
        if(item.status=="created")
        {
      
        if(item.paritications===null)
        {
            
            this.joinGroupFromId(item.group_id);    
                
        }
        else
        {
            console.log("done")
            let paritications=item.paritications;
            let is_participant=0;
            for(let i in paritications)
            {
                console.log(paritications[i].uid)
                if(paritications[i].uid==auth().currentUser.uid)
                {
                    console.log("already here")
                    is_participant=1;
                    break;

                }
                
            }

            if(!is_participant)
            {
                this.joinGroupFromId(item.group_id);    
            }
            else
            {
                this.props.navigation.navigate("Prechat",{group_id:item.group_id});
            }
        }

    }

    }

    checkAdminButton()
    {
        const groupDataCopy=this.state.groupData;
        
        if(groupDataCopy)
        {
            if(groupDataCopy.created_id===auth().currentUser.uid)
             {
                return(
                    <View style={styles.button}>
                    <Button title="Start" onPress={this.startGroup} />
                    </View>
                )
            }
        }
       
    }
    startGroup=()=>
    {
        this.setState({
            is_loading:true
        })
        this.updateParticipantsDB(this.props.route.params.group_id).then(is_update=>{
    if(is_update)
    {

        database()
                .ref('/groups/'+this.props.route.params.group_id)
                .update({status:'started'})
                .then(() => {
                  //  this.props.navigation.navigate("Chat",{group_id:this.props.route.params.group_id})

                        this.getDeviceTokens().then(deviceTokens=>{
                            let groupData=this.state.groupData;
                            let payload={
                                type:"chart_started",
                                group_id:this.props.route.params.group_id
                            }
                            sendPushNotification(deviceTokens,groupData.group+" Chart Started","Chart has been started . Please join",payload)
                            this.setState({
                                is_loading:false
                            })
                            this.props.navigation.navigate("Chat",{group_id:this.props.route.params.group_id})
                       
                        
                    })
               
                
                });
            }
            else
            {
                alert("Sorry!Something went wrong.")
            }
            });
                
    }

    async getDeviceTokens()
    {
       const userData=await database()
                .ref('/users')
                .once('value').then((res)=>{
                    return res.val()
                })
                
        let joined_users=this.state.joined_users;
        let deviceTokens=[];
        let currentUserName=this.state.username;
       
        for(let i in userData)
        {
          for(let j in joined_users)
          {
               
            if(userData[i].username.toLowerCase()==joined_users[j].toLowerCase() && userData[i].username.toLowerCase()!==currentUserName)
            {
                deviceTokens.push(userData[i].deviceToken)
            }
            }
                    
        }
        console.log("at function end")
        return deviceTokens
    }

    joinedUsers()
    {
        const joined_users=this.state.joined_users;
        if(joined_users)
        {
            return(
                    joined_users.map(item=>(
                    <View style={{padding:5,backgroundColor:'#13fa23',marginTop:10}} key={item}><Text style={{textAlign:"center",fontSize:16}}>{item}</Text></View>
                    ))

                )
        
        }
        else
        {
            return null
        }
        
    }

    checkJoinButton()
    {
        const groupDataCopy=this.state.groupData;
        if(groupDataCopy)
        {
            if(groupDataCopy.status=="created")
            {
                let paritications=groupDataCopy.paritications;
                let is_participation=0;
              
                for(let i in paritications)
                {
                    if(paritications[i].uid===auth().currentUser.uid)
                    {
                        is_participation=1;
                        break;
                    }
                    
                }
                return(
                <View style={styles.button}><Button title={"Join"} disabled={is_participation?true:false} onPress={()=>this.joinGroupFromId(this.props.route.params.group_id)}/>
                </View>
                )
            }
            else
            {
                return null;
            }
           
        }
       
    }

    updateParticipants=()=>{
        this.updateParticipantsDB(this.props.route.params.group_id)
    }
    
    shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }

    async updateParticipantsDB(group_id){

        const grpData=await database()
                .ref('/groups/'+group_id)
                .once('value').then((res)=>{

                    return res.val()
                });
      
       

        let participants=grpData.paritications;
        let joinUsersKeys=[];

        for(let i in participants)
        {
            joinUsersKeys.push(i);
        }

        joinUsersKeys=this.shuffle(joinUsersKeys)
    
        let turn=0;
        let updateData={}
        let is_turn=0;
       
        let turn_time=moment().tz("America/New_York").format('YYYY-MM-DD HH:mm:ss');
        for(let jUser in joinUsersKeys)
        {
            is_turn=0
           
                if(turn===0)
                    is_turn=1
            turn=turn+1
           
        updateData[joinUsersKeys[jUser]+"/is_turn"]=is_turn
        updateData[joinUsersKeys[jUser]+"/turn_time"]=turn_time
        updateData[joinUsersKeys[jUser]+"/turnno"]=turn
       

        }
        
       
      const is_update=await database().ref("groups/"+group_id+"/paritications").update(
            updateData
        ).then(()=>{
                return true
            })

        return is_update;


    }

    sendNotification=()=>{
        
        this.getDeviceTokens().then(deviceTokens=>{
            let groupData=this.state.groupData;
            sendPushNotification(deviceTokens,groupData.group+" Chart Started","Chart has been started . Please join","chat_to_start")
        })
       
     //   sendPushNotification(deviceTokens,"Testing Title","testing body","chat_to_start")

    }

    render()
    {
       
        if(this.state.loading)
        {   return(
            <View style={styles.loading}>
            <ActivityIndicator/>
           </View>
            )
        }
        else
        {
            
        return (

            <ScrollView>
                    <View style={styles.sectionContainer}>
                       
                        {this.checkAdminButton()}    
                        {this.checkJoinButton()}        
                       <Button title={"Send Notfication"}
                       onPress={this.sendNotification}
                       />
                       <Button title={"Setting Participants"}
                       onPress={this.updateParticipants}
                       />
                        <Text style={{textAlign:"center",fontSize:16}}>Joined Users</Text>
                        {this.joinedUsers()}

                     
                    </View>
            </ScrollView>
                    
        );
        }
    }
};

const styles = StyleSheet.create({
  
  sectionContainer: {
    flex:1,
    backgroundColor:'#fff',
    padding:50
  },
  input:{
    width:'100%',
    borderBottomWidth:3,
    borderBottomColor:'#eaeaea',
    fontSize:16,
    padding:5,
    marginTop:0,
    marginBottom:10,
},
button:{
    marginTop:20,
    marginBottom:10

},
loading:{
    flex:1,
    backgroundColor:'#fff',
    alignItems:"center",
    justifyContent:"center"
  },
  errorContainer:{
    marginTop:30,
    marginBottom:10,
    padding:10,
    backgroundColor:'#f44336'
},
errorLabel:{
    color:'#fff',
    textAlign:"center",
    textAlignVertical:"center"
    
}
  
});



export default PreChatComponent;
