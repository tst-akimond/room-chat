const Validation =(value,rules,form) =>{

    let valid=true;

    for(let rule in rules)
    {
        
        switch(rule)
        {
           
            case 'isEmail':
                 
                valid=valid && checkEmail(value);
                break;
             case 'isRequired':
              
                valid=valid && checkRequired(value);
              
             
                break;
            case 'minLength':
                valid=valid && validateMinLength(value,rules[rule]);
                break;

            case 'confirmPass':
                valid=valid && validDateConfirmPass(value,form[rules[rule]].value);
                break;

            default:
                valid=true
        }

    }

    return valid

}

const checkRequired=value=>{
   
    if(value.trim()!="")
        return true
    
    return false;
}

const checkEmail=email=>{
    
    const expression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
    return expression.test(String(email).toLocaleLowerCase())
}

const validateMinLength =(value,ruleValue) => {
    if(value.length>=ruleValue)
        return true
    return false
}

const validDateConfirmPass=(value,ruleValue)=>{
    return value===ruleValue?true:false

}


export default Validation