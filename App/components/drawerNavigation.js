import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator,DrawerContentScrollView,DrawerItemList,DrawerItem} from '@react-navigation/drawer';
import Home from './home';
import auth from '@react-native-firebase/auth';
const Drawer = createDrawerNavigator();

const drawerCompoenet=()=>
{
    return(
        <Drawer.Navigator drawerContent={props => {
            return (
              <DrawerContentScrollView {...props}>
                <DrawerItemList {...props} />
                <DrawerItem label="Logout" onPress={() => auth()
                            .signOut()
                            .then(() => console.log('User signed out!'))} />
              </DrawerContentScrollView>
            )
          }}>
        <Drawer.Screen name="Home" component={Home} />
        
          </Drawer.Navigator>

    )
}

export default drawerCompoenet;