import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button, ActivityIndicator, ScrollView, TouchableOpacity, Image, Dimensions, FlatList } from 'react-native';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import Timer from 'react-compound-timer';
import axios from 'axios'
import 'moment-timezone';
import moment from 'moment';
import SoundPlayer from 'react-native-sound-player'
import Modal from 'react-native-modal';
import { Picker } from '@react-native-community/picker';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Icon1 from 'react-native-vector-icons/dist/FontAwesome5';
import Icon2 from 'react-native-vector-icons/dist/MaterialIcons';
import Icon3 from 'react-native-vector-icons/dist/FontAwesome';
import Icon4 from 'react-native-vector-icons/dist/AntDesign';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
const TextArr = [
    {
        id: 0,
        text: 'Yes!'
    },
    {
        id: 1,
        text: 'Good game!'
    },
    {
        id: 2,
        text: 'Lucky roll!'
    },
    {
        id: 3,
        text: 'Faster Please'
    },
    {
        id: 4,
        text: 'Well Played'
    },
    {
        id: 5,
        text: 'Please don\'t kill'
    },
    {
        id: 6,
        text: 'Yes!'
    },
    {
        id: 7,
        text: 'Good game!'
    },
    {
        id: 8,
        text: 'Lucky roll!'
    },
    {
        id: 9,
        text: 'Faster Please!'
    },
    {
        id: 10,
        text: 'Well Played!'
    },
    {
        id: 11,
        text: 'Please don\'t Kill'
    },
]
class ChatComponent extends React.Component {
    _menu = null;
    _isMounted = false;
    _playSound = false;
    _is_off_Sound = false;
    state = {
        paritications: "",
        messages: "",
        message: "",
        username: "",
        user_waiting: "",
        currentTurn: "",
        currentKey: "",
        is_turn: 0,
        offchatInput: "",
        visible: false,
        off_chat_messages: null,
        userData: "",
        selectUser: 0,
        joinUsers: null,
        reportReason: "Abusive Language",
        reportModal: false,
        alreadyReportedUser: [],
        is_user_report: 0
    }

    revArray = (firebaseMessages) => {
        let userMessages = firebaseMessages;
        let msgs = [];
        for (let msk in userMessages) {
            msgs.push(userMessages[msk])
        }
        let msgData = [];
        let rvArray = [];
        for (let i = msgs.length - 1; i >= 0; i--) {
            rvArray.push(msgs[i])
        }

        return rvArray;

    }

    async getJoinedUser() {
        let userData = await database().ref('/groups/' + this.props.route.params.group_id + "/paritications").once('value').then(snapshot => {
            return snapshot.val();
        })


        let joinUsers = []
        for (let user in userData) {
            if (userData[user].uid != auth().currentUser.uid) {
                joinUsers.push({ username: userData[user].username, user_id: userData[user].uid })

            }
        }

        return joinUsers

    }
    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };
    componentDidMount() {
        this._isMounted = true;

        let turn_time = moment().tz("America/New_York").format('YYYY-MM-DD HH:mm:ss');



        this._interval = setInterval(() => {

            axios.get("https://balwinder.xyz/firebase/firebase.php?group_id=" + this.props.route.params.group_id).then((res) => {
                return res.data
            }).catch((e) => {

            });


        }, 8000)


        this.reportHandler = database()
            .ref('/groups/' + this.props.route.params.group_id + "/reports")
            .on('value', snapshot => {
                if (!snapshot)
                    return;


                //get total pertificaption
                this.getJoinedUser().then(joinUsers => {



                    let joinUser_count = joinUsers.length;

                    //check if current user has report more than on rqual to 50%;
                    let reportdata = snapshot.val();
                    if (reportdata) {
                        let alreadyReportedUser = []

                        for (let rpdata in reportdata) {
                            let reported_by = reportdata[rpdata].reported_by

                            for (let repObject in reported_by) {

                                if (reported_by[repObject].uid == auth().currentUser.uid) {
                                    alreadyReportedUser.push(rpdata);
                                    break;
                                }
                            }
                        }

                        let currentUserReportedData = reportdata[this.state.username];
                        let is_user_report = 0;
                        if (currentUserReportedData) {
                            console.log("current report");
                            console.log(currentUserReportedData)
                            let currentReport = currentUserReportedData.reports;

                            if (currentReport >= (joinUser_count / 2)) {
                                is_user_report = 1
                            }
                        }
                        let rsJoinUsers = [];

                        for (let jUser in joinUsers) {

                            if (alreadyReportedUser.indexOf(joinUsers[jUser].username) == "-1") {

                                rsJoinUsers.push(joinUsers[jUser])
                            }
                        }

                        if (this._isMounted) {
                            this.setState({
                                alreadyReportedUser: alreadyReportedUser,
                                is_user_report: is_user_report,
                                joinUsers: rsJoinUsers
                            })
                        }
                    }
                    else {
                        this.setState({
                            joinUsers: joinUsers
                        })
                    }
                })

            });

        database()
            .ref('/users/' + auth().currentUser.uid)
            .once('value', snapshot => {
                let uData = snapshot.val()



                this.setState({
                    username: uData.username
                })

            })

        this.offmsgHandler = database()
            .ref('/groups/' + this.props.route.params.group_id + "/off_chat_messages")
            .on('value', snapshot => {
                if (!snapshot)
                    return

                let off_chat_messages = snapshot.val();
                let rvArray = this.revArray(off_chat_messages);

                if (this._is_off_Sound) {
                    SoundPlayer.playSoundFile('blop', 'wav')
                }
                this._is_off_Sound = true;

                if (this._isMounted) {

                    this.setState({
                        off_chat_messages: rvArray
                    })

                }
            })

        this.msgHandler = database()
            .ref('/groups/' + this.props.route.params.group_id + "/messages")
            .on('value', snapshot => {
                if (!snapshot)
                    return

                let userMessages = snapshot.val();
                if (this._playSound) {

                    SoundPlayer.playSoundFile('blop', 'wav')
                }


                this._playSound = true;
                let rvArray = this.revArray(userMessages);
                if (this._isMounted) {

                    this.setState({
                        messages: rvArray
                    })
                }


            })

        this.groupHandler = database()
            .ref('/groups/' + this.props.route.params.group_id + "/paritications")
            .on('value', snapshot => {
                if (!snapshot)
                    return

                let paritications = snapshot.val();


                let particArray = [];


                let currentTurn, currentKey, userData;
                for (let i in paritications) {
                    paritications[i]['participation_key'] = i;
                    particArray.push(paritications[i]);
                    if (paritications[i].is_turn === 1) {
                        currentTurn = paritications[i].turnno;
                        currentKey = paritications[i]['participation_key'];
                    }
                    if (paritications[i].uid === auth().currentUser.uid) {

                        userData = paritications[i];

                    }
                }

                if (userData) {
                    let is_turn = 0;
                    if (userData.is_turn === 1) {
                        is_turn = 1;

                    }

                    if (this._isMounted) {
                        this.setState({
                            is_turn: is_turn,
                            userData: userData,
                            is_participation: 1
                        })

                    }

                }
                if (this._isMounted) {
                    this.setState({
                        paritications: particArray,
                        currentTurn: currentTurn,
                        currentKey: currentKey
                    })
                }
            })
    }
    componentWillUnmount() {
        this._playSound = false;
        this._isMounted = false;
        this.reportHandler();
        this.groupHandler();
        this.msgHandler();
        this.offmsgHandler();
        clearInterval(this._interval);

    }
    offChatSubmit = () => {
        let message = this.state.offchatInput;

        if (message.trim() === "") {
            return;
        }
        let messageToSubmit = { message: message, uid: auth().currentUser.uid, username: this.state.username }
        const newRef = database()
            .ref('/groups/' + this.props.route.params.group_id + '/off_chat_messages')
            .push(messageToSubmit).then(() => {
                this.setState({
                    offchatInput: "",
                })
            })
    }
    async settingNextTurn(userData, group_id) {
        this.setState({
            is_turn: 0
        })
        let turnno = this.nextTurn(userData.turnno);
        let pariticationsCopy = this.state.paritications;
        let partDatatoupdate = {};
        let turn_time = moment().tz("America/New_York").format('YYYY-MM-DD HH:mm:ss');
        for (let partcp in pariticationsCopy) {
            if (pariticationsCopy[partcp].participation_key == turnno) {
                partDatatoupdate[turnno + "/is_turn"] = 1;
                partDatatoupdate[turnno + "/turn_time"] = turn_time;
            }
            else {
                partDatatoupdate[pariticationsCopy[partcp].participation_key + "/is_turn"] = 0;
            }
        }
        let response = await database().ref('/groups/' + group_id + "/paritications").update(partDatatoupdate).then(() => {
            return true;
        })
        return response;
    }
    sendMessage = (userData) => {
        this.setState({
            is_turn: 0
        })
        let message = this.state.message;
        if (message.trim() === "")
            return
        let messageToSubmit = { message: message, uid: auth().currentUser.uid, username: this.state.username }
        const newRef = database()
            .ref('/groups/' + this.props.route.params.group_id + '/messages')
            .push(messageToSubmit).then(() => {
                this.settingNextTurn(userData, this.props.route.params.group_id).then((res) => {
                    this.setState({
                        message: ""
                    })
                })
            })
    }
    getOffChatMessages = () => {
        let messagesCopy = this.state.off_chat_messages
        if (messagesCopy && messagesCopy != "") {
            return (
                messagesCopy.map((item, key) => (
                    <View key={key} style={{
                        marginTop: 10,
                        marginHorizontal: 15,
                        backgroundColor: 'rgb(255, 251, 247)',
                        borderRadius: 10,
                    }}>
                        <Text style={{
                            fontSize: 16,
                            color: '#000',
                            paddingTop: 15,
                            paddingHorizontal: 15,
                            fontWeight: "bold"
                        }}>
                            {item.message}
                        </Text>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignSelf: 'flex-end',
                            paddingRight: 15,
                            paddingVertical: 5,
                        }}>
                            <View style={{
                                marginHorizontal: 5,
                            }}>
                                <Image style={styles.avatar1} source={{ uri: 'https://w7.pngwing.com/pngs/725/525/png-transparent-woman-wearing-blue-crew-neck-long-sleeved-top-illustration-cartoon-gesture-illustration-beautiful-girl-fashion-girl-beautiful-vector-fashion.png' }} />
                                <View style={{
                                    backgroundColor: 'green',
                                    width: 6,
                                    height: 6,
                                    borderRadius: 6 / 2,
                                    position: 'absolute',
                                    bottom: 0,
                                    right: 0,
                                }}></View>
                            </View>
                            <Text style={{
                                fontSize: 12,
                                color: '#000',
                                fontWeight: "bold"
                            }}>
                                {item.username}
                            </Text>
                        </View>
                    </View>
                ))

            )
        }
        else {
            return (
                <Text style={{ textAlign: "center", fontSize: 18, marginTop: 10 }}>No Chat Message Available</Text>
            )
        }
    }
    getMessages = () => {
        let messagesCopy = this.state.messages
        if (messagesCopy) {
            return (
                messagesCopy.map((item, key) => (
                    <View key={key} style={{
                        marginTop: 20,
                        marginHorizontal: 15,
                        backgroundColor: 'rgb(255, 251, 247)',
                        borderRadius: 10,
                    }}>
                        <Text style={{
                            fontSize: 16,
                            color: '#000',
                            paddingTop: 15,
                            paddingHorizontal: 15,
                            fontWeight: "bold"
                        }}>
                            {item.message}
                        </Text>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            alignSelf: 'flex-end',
                            paddingRight: 15,
                            paddingVertical: 5,
                        }}>
                            <View style={{
                                marginHorizontal: 5,
                            }}>
                                <Image style={styles.avatar1} source={{ uri: 'https://w7.pngwing.com/pngs/725/525/png-transparent-woman-wearing-blue-crew-neck-long-sleeved-top-illustration-cartoon-gesture-illustration-beautiful-girl-fashion-girl-beautiful-vector-fashion.png' }} />
                                <View style={{
                                    backgroundColor: 'green',
                                    width: 6,
                                    height: 6,
                                    borderRadius: 6 / 2,
                                    position: 'absolute',
                                    bottom: 0,
                                    right: 0,
                                }}></View>
                            </View>
                            <Text style={{
                                fontSize: 12,
                                color: '#000',
                                fontWeight: "bold"
                            }}>
                                {item.username}
                            </Text>
                        </View>
                    </View>
                ))

            )
        }
    }
    nextTurn = (turnno) => {
        let pariticationsCopy = this.state.paritications;
        let total_turns = pariticationsCopy.length;
        let turn_no;
        for (let partcp in pariticationsCopy) {
            if (total_turns === turnno && pariticationsCopy[partcp].turnno === 1) {
                turn_no = pariticationsCopy[partcp].participation_key
                break;
            }
            else if (pariticationsCopy[partcp].turnno === turnno + 1) {
                turn_no = pariticationsCopy[partcp].participation_key;
                break;
            }
        }
        return turn_no;
    }
    getInput = () => {
        let pariticationsCopy = this.state.paritications;
        let currentTurn = this.state.currentTurn
        let userData = this.state.userData;

        let is_participation = this.state.is_participation;
        let is_turn = this.state.is_turn;
        let total_turns = pariticationsCopy.length;
        let next_turn;
        if (is_participation) {
            let is_user_report = this.state.is_user_report;
            if (is_turn == 1 && is_user_report === 0) {
                let turn_time = moment().tz("America/New_York").format('YYYY-MM-DD HH:mm:ss');
                turn_time = turn_time.replace(" ", "T")
                let user_turn = userData.turn_time.replace(" ", "T").trim()
                let d1 = new Date(turn_time)
                let d = new Date(user_turn)
                let diff = (60 - ((d1 - d) / 1000))
                return (
                    <View style={styles.footer}>
                        <View style={styles.messageinput}>
                            <TextInput
                                multiline={true}
                                style={styles.input}
                                value={this.state.message}
                                placeholder={"Type a message here"}
                                onChangeText={(val) => this.setState({ message: val })} />
                            <TouchableOpacity
                                onPress={() => this.sendMessage(userData)}>
                                <Icon3 name="send" size={25} color="gray" />
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }
            else {
                return (
                    <View style={styles.footer}>
                        <View style={styles.messageinput}>
                            <Text style={{ fontSize: 15, textAlign: "center", marginLeft: 20, }}>Waiting for your turn</Text>
                        </View>
                    </View>
                )
            }
        }
    }
    showTimercounts = () => {
        let pariticationsCopy = this.state.paritications;
        let currentTurn = this.state.currentTurn
        let userData = this.state.userData;
        let is_participation = this.state.is_participation;
        let is_turn = this.state.is_turn;
        let total_turns = pariticationsCopy.length;
        let next_turn;
        if (is_participation) {
            let is_user_report = this.state.is_user_report;
            if (is_turn == 1 && is_user_report === 0) {
                let turn_time = moment().tz("America/New_York").format('YYYY-MM-DD HH:mm:ss');
                turn_time = turn_time.replace(" ", "T")
                let user_turn = userData.turn_time.replace(" ", "T").trim()
                let d1 = new Date(turn_time)
                let d = new Date(user_turn)
                let diff = (60 - ((d1 - d) / 1000))
                return (
                    <View style={styles.timercontainer}>
                        <Timer
                            initialTime={diff * 1000}
                            direction="backward"
                            timeToUpdate={10}
                            checkpoints={[
                                {
                                    time: 0,
                                    callback: () => this.settingNextTurn(userData, this.props.route.params.group_id),
                                },
                            ]}
                        >
                            <Text style={styles.timertext}>
                                <Text style={{ fontSize: 20 }}>
                                    <Timer.Seconds />
                                </Text>
                            </Text>
                        </Timer>
                    </View>
                )
            }
            else {
                return (
                    <View style={styles.timercontainer}>
                    </View>
                )
            }
        }
    }
    turnUserName = () => {
        let pariticationsCopy = this.state.paritications;
        for (let partcp in pariticationsCopy) {
            if (pariticationsCopy[partcp].is_turn === 1)
                return pariticationsCopy[partcp].username;
        }
        return "";
    }
    getMenu = () => {
        return (
            this.state.is_participation ? <React.Fragment><Text style={{ textAlign: "center", fontSize: 18 }}>{this.turnUserName()} is typing....</Text><Button
                title={"Chat"} onPress={() => this.setState({ visible: true })} />
                <Button
                    title={"Report User"} onPress={() => this.setState({ reportModal: true })} /></React.Fragment>
                : null
        )
    }
    offChatBtnHander = (msg) => {
        this.setState({ offchatInput: msg }, () => {
            this.offChatSubmit()
        });
        // this.offChatSubmit(msg)
    }
    getUserSelect = () => {
        let joinUsers = this.state.joinUsers;
        let alreadyReportedUser = this.state.alreadyReportedUser
        var radio_props = [
            { label: 'Abusive Language', value: "Abusive Language" },
            { label: 'Not Participating', value: "Not Participating" },
            { label: 'Typing Random Messages', value: "Typing Random Messages" }
        ];
        if (joinUsers) {
            let report_to_user_exist = 0;
            console.log(joinUsers)
            let PickerItems = joinUsers.map((item, key) => {
                report_to_user_exist = 1
                return <Picker key={key} value={key} label={item.username} />
            })
            if (!report_to_user_exist) {
                return (
                    <View>
                        <Text>There is not user to report</Text>
                    </View>
                )
            }
            return (
                <View>
                    <Text style={{
                        fontSize: 2
                    }}>User:</Text>
                    <Picker
                        selectedValue={this.state.selectUser}
                        style={{ height: 50, width: 200 }}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({ selectUser: itemIndex })
                        }
                        }>
                        {PickerItems}
                    </Picker>
                    <RadioForm
                        radio_props={radio_props}
                        initial={0}
                        onPress={(value) => { this.setState({ reportReason: value }) }}
                    />
                    <Button
                        title="Submit"
                        onPress={this.sbmtreportHandler}
                    />
                </View>
            )
        }
    }
    sbmtreportHandler = async () => {
        let joinUser = this.state.joinUsers;
        let reportUser = joinUser[this.state.selectUser];
        console.log(reportUser);
        if (joinUser !== "") {
            let userReportData = await database()
                .ref('/groups/' + this.props.route.params.group_id + '/reports/' + reportUser.username).
                once('value').then(snapshot => {
                    return snapshot.val();
                });
            let reports, reported_by = {}, reportData = []
            if (!userReportData) {
                reports = 1;
                reported_by.uid = auth().currentUser.uid;
                reported_by.username = this.state.username;
                reported_by.reason = this.state.reportReason;
                reportData.push(reported_by)
            }
            else {
                reports = userReportData.reports + 1
                reportData = userReportData.reported_by
                reported_by.uid = auth().currentUser.uid;
                reported_by.username = this.state.username;
                reported_by.reason = this.state.reportReason;
                reportData = [...reportData, reported_by]
            }
            let report_to_submit = {
                reports: reports,
                reported_by: reportData
            }
            const newRef = database()
                .ref('/groups/' + this.props.route.params.group_id + '/reports/' + reportUser.username)
                .set(report_to_submit).then(() => {
                    alert("you reported this user successfully")
                })
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal isVisible={this.state.reportModal}
                    animationIn={'slideInUp'}
                    animationInTiming={500}
                >
                    <View style={{ padding: 15, backgroundColor: "white" }}>
                        <TouchableOpacity style={{
                            alignSelf: 'flex-end',
                            padding: 10,
                        }} onPress={() => this.setState({ reportModal: false })}>
                            <Icon4 name="closecircleo" size={25} color="gray" />
                        </TouchableOpacity>
                        {this.getUserSelect()}
                    </View>
                </Modal>
                <Modal isVisible={this.state.visible}
                    animationIn={'slideInUp'}
                    animationInTiming={500}
                    style={{ justifyContent: "flex-end", margin: 0 }}
                >
                    <View style={styles.modalContent}>
                        <TouchableOpacity style={{
                            alignSelf: 'flex-end',
                            paddingRight: 15,
                        }} onPress={() => this.setState({ visible: false })}>
                            <Icon4 name="closecircleo" size={25} color="gray" />
                        </TouchableOpacity>
                        <ScrollView style={{ height: 190 }}>
                            {this.getOffChatMessages()}
                            <View style={{ height: 10 }}></View>
                        </ScrollView>
                        <FlatList
                            numColumns={3}
                            data={TextArr}
                            renderItem={({ item }) =>
                                <TouchableOpacity
                                    onPress={() => this.offChatBtnHander(item.text)}
                                    style={{
                                        width: Dimensions.get('window').width * 0.3,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        height: 40,
                                        backgroundColor: 'rgb(149, 124, 94)',
                                        borderRadius: 10,
                                        margin: 5,
                                    }}>
                                    <Text style={{
                                        color: 'white',
                                        fontSize: 15,
                                        fontWeight: 'bold',
                                        textAlign: 'center',
                                    }}>
                                        {item.text}
                                    </Text>
                                </TouchableOpacity>
                            }
                            keyExtractor={item => item.id}
                        />
                        <View style={[styles.messageinput, { alignSelf: 'center', marginVertical: 5 }]}>
                            <TextInput
                                onChangeText={(value) => this.setState({ 'offchatInput': value })}
                                value={this.state.offchatInput}
                                style={styles.input}
                                placeholder="Type a message here"
                            />
                            <TouchableOpacity
                                onPress={this.offChatSubmit}>
                                <Icon3 name="send" size={25} color="gray" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View style={styles.header}>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                    }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Home')}
                            style={{
                            }}>
                            <Icon1 name="chevron-left" size={25} color="gray" />
                        </TouchableOpacity>
                        <View style={{
                            marginHorizontal: 15,
                        }}>
                            <Image style={styles.avatar} source={{ uri: 'https://w7.pngwing.com/pngs/725/525/png-transparent-woman-wearing-blue-crew-neck-long-sleeved-top-illustration-cartoon-gesture-illustration-beautiful-girl-fashion-girl-beautiful-vector-fashion.png' }} />
                            <View style={{
                                backgroundColor: 'green',
                                width: 8,
                                height: 8,
                                borderRadius: 8 / 2,
                                position: 'absolute',
                                bottom: 0,
                                right: 0,
                            }}></View>
                        </View>
                        {
                            this.showTimercounts()
                        }
                    </View>
                    <Menu
                        ref={this.setMenuRef}
                        button={
                            <TouchableOpacity onPress={this.showMenu}>
                                <Icon2 name="more-horiz" size={35} color="gray" />
                            </TouchableOpacity>
                        }>
                        <MenuItem onPress={() => { this.hideMenu(); this.setState({ visible: true }) }}>CHAT</MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={() => { this.hideMenu(); this.setState({ reportModal: true }) }}>REPORT USER</MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={this.hideMenu}>CLOSE CHAT</MenuItem>
                    </Menu>
                </View>
                {/* <View style={{ marginBottom: 5, borderBottomWidth: 2, borderBottomColor: '#ccc' }}>
                    {this.getMenu()}
                </View> */}
                <ScrollView>
                    {this.getMessages()}
                    <View style={{ height: 30 }}></View>
                </ScrollView>
                {this.getInput()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgb(253, 243, 216)',
        flex: 1,
    },
    modalContent: {
        backgroundColor: "white",
        paddingTop: 15,
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
        height: 500,
        width: Dimensions.get('window').width,
    },
    avatar: {
        width: 40,
        height: 40,
        resizeMode: 'cover',
        borderRadius: 40 / 2,
    },
    avatar1: {
        width: 20,
        height: 20,
        resizeMode: 'cover',
        borderRadius: 20 / 2,
    },
    timercontainer: {
        width: 45,
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 45 / 2,
        // borderWidth: 2,
        // borderColor: 'green',
    },
    timertext: {
        fontFamily: 'Helvetica Neue',
        textAlign: "center",
        alignItems: "center"
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 60,
        paddingHorizontal: 15,
        backgroundColor: 'white',
    },
    footer: {
        backgroundColor: 'white',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: '85%',
        fontSize: 15,
        marginLeft: 20,
    },
    messageinput: {
        flexDirection: 'row',
        width: '95%',
        alignItems: 'center',
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 50 / 2,
        height: 50,
    }
})

export default ChatComponent