import React from 'react';
import { Image, View, Text, StyleSheet } from 'react-native';

import WelcomeSingle from './WelcomeSingle';
import WelcomeSlide from './WelcomeSlide';


const slides = [
  {'title': 'All Your Favourite Dishes', 'image': false, 'content': 'Order from your nearby best resturant in the town.'},
  {'title': 'See Everything', 'image': false, 'content': 'You can enjoy your entertainment'},
  {'title': 'Find your friends', 'image':false, 'content': 'Awesome chat with friends'},
 ];

class WelcomeTutorial extends React.Component {
 
  constructor(props) {
    super(props);

    this.state = {
      slides : slides
    }
  }
  
	render() {
    
		return (
      !this.state.slides || this.state.slides.length === 0 ?
        <View
          style={this.props.style}
        />
      :
        this.state.slides.length === 1 ?
        <WelcomeSingle
          slide={this.state.slides[0]}
          style={this.props.style}
        />
        :
        <WelcomeSlide
          slides={this.state.slides}
          style={this.props.style}
        />
      
		);
	}
}

var styles = StyleSheet.create({
});

export default WelcomeTutorial;
