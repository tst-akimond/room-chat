import React from 'react';
import { View, Text, Image, TouchableHighlight, TouchableOpacity, Linking, StyleSheet, Animated, Dimensions } from 'react-native';

import { Values } from '../../../config';

class WelcomeSingle extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var { slide } = this.props;

    return (
      <View style={styles.swiper}>

        <View style={styles.slideContainer}>
          {slide.image && <Image style={{ tintColor: 'white' }} source={slide.image} />}
          <View style={{ marginBottom: 8 }}>
            <Text style={[styles.slideTitleBig, { color: 'white' }]}>
              {slide.title.toUpperCase()}
            </Text>
          </View>
          <Text style={styles.slideCopy}>
            {slide.content}
          </Text>
        </View>
      </View>

    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  gradient: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
  },
  swiper: {
    flex: 1,
  },
  slideContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 32,
    paddingRight: 32,
  },
  slideTitleBig: {
    fontSize: 32,
    fontWeight: '900',
    letterSpacing: 0.25,
  },
  slideTitle: {
    fontSize: 20,
    fontWeight: '900',
    letterSpacing: 0.25,
  },
  slideCopy: {
    textAlign: 'center',
    lineHeight: 24,
    color: 'white',
  },


});

export default WelcomeSingle;
