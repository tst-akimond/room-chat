import React from 'react';
import { View, Text, Image, TouchableHighlight, TouchableOpacity, Linking, StyleSheet, Animated, Dimensions } from 'react-native';

import ViewPager from '@react-native-community/viewpager';

import { Values } from '../../../config';

class WelcomeSlide extends React.Component{
    constructor(props) {
        super(props);
    
        this._scrollViewHorizontalPos = new Animated.Value(0);
        this.state = {
          activeSlide: 0,
        }
    }
 
	render() {
    var { slides } = this.props;

    var left = -10000;
    left = this._scrollViewHorizontalPos.interpolate({
        inputRange: Object.keys(slides),
        outputRange: slides.map((tab, i) =>  i * 16),
        extrapolate: 'clamp'
      });
     
    overlayOpacityVal = this._scrollViewHorizontalPos.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0],
      extrapolate: 'clamp'
    });
		return (
            <View style={styles.container}>

                <Animated.View style={[ styles.overlay, {opacity: overlayOpacityVal} ]}/>
                <View style={styles.swiper}>
                <ViewPager
                    style={{flex: 1}}
                    initialPage= {0}
                    onPageSelected= {e => {
                      this.setState({activeSlide: e.nativeEvent.position});
                      console.log("position", e.nativeEvent.position)
                    }}
                    onPageScroll={e => {
                      this._scrollViewHorizontalPos.setValue(e.nativeEvent.offset + e.nativeEvent.position)
                    }}
                >
                    {
                    slides.map((slide, index) => {
                        return (
                            <View style={styles.slideContainer} key={index}>
                            {slide.image && <Image style={{tintColor: 'white'}} source={slide.image} />}
                            <View style={{ marginBottom: 8 }}>
                                <Text style={[ styles.slideTitle, {color: 'white'} ]}>
                                {slide.title.toUpperCase()}
                                </Text>
                            </View>
                            <Text style={styles.slideCopy}>
                                {slide.content}
                            </Text>
                            </View>
                        );
                    })
                    }
                </ViewPager>

                <Animated.View style={styles.paginationOuterContainer}>
                    <View style={styles.paginationInnerContainer}>
                    <Animated.View style={[ styles.paginationDotSelector, {left: left, borderColor: 'white'} ]} />
                        {
                        slides.map((slide, index) => {
                            var isTabActive = index === this.state.activeSlide;
                            return (
                                <View key={index}>
                                <View style={[ styles.paginationDot, {opacity: isTabActive ? 1 : 0.4} ]}/>
                                </View>
                            );
                        })
                        }
                    </View>
                </Animated.View>
                </View>

            </View>
		);
	}
}

var styles = StyleSheet.create({
      container: {
        flex: 1,
      },
      overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'black',
      },
      gradient: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'transparent',
      },
      swiper: {
        flex: 1,
      },
      slideContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32,
      },
      slideTitleBig: {
        fontSize: 32,
        fontWeight: '900',
        letterSpacing: 0.25,
      },
      slideTitle: {
        fontSize: 20,
        fontWeight: '900',
        letterSpacing: 0.25,
      },
      slideCopy: {
        textAlign: 'center',
        lineHeight: 24,
        color: 'white',
      },
      artistNameContainer: {
        position: 'absolute',
        width: Values.deviceWidth,
      },
      artistName: {
        textAlign: 'center',
        fontSize: 44,
        fontWeight: '700',
        letterSpacing: -1,
        color: 'white',
        shadowColor: 'black',
        shadowOpacity: 0.5,
        shadowOffset: {
          height: 0,
          width: 0,
        },
      },
      connectButtonsContainer: {
        flex: 2,
        justifyContent: 'center',
        marginLeft: 24,
        marginRight: 24,
      },
      logInContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 16,
      },
      logInLink: {
        fontSize: 13,
        fontWeight: '600',
        color: 'rgba(255,255,255,0.8)',
      },
      termsPrivacyContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      termsPrivacyTextInnerContainer: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 4,
      },
      termsPrivacyText: {
        fontSize: 11,
        color: 'rgba(255,255,255,0.4)',
      },
      paginationOuterContainer: {
        position: 'absolute',
        bottom: 24,
        width: Values.deviceWidth,
        height: 20,
        backgroundColor: 'transparent',
      },
      paginationInnerContainer: {
        height: 24,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    
      },
      paginationDot: {
        width: 8,
        height: 8,
        margin: 4,
        backgroundColor: 'white',
        borderRadius: 4,
      },
      paginationDotSelector: {
        position: 'absolute',
        top: 4,
        width: 16,
        height: 16,
        backgroundColor: 'transparent',
        borderWidth: 2,
        borderRadius: 8,
      },
});

export default WelcomeSlide;
