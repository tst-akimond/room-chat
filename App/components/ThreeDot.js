import React from 'react'
import {
  Text,
  View,
  StyleSheet
} from 'react-native'
const styles = StyleSheet.create({
  act_dot: {
    width: 9, height: 9, borderRadius: 9 / 2, margin: 3, backgroundColor: 'rgb(241, 98, 0)'
  },
  inact_dot: {
    width: 8, height: 8, borderRadius: 8 / 2, margin: 3, backgroundColor: 'rgb(252, 231, 215)'
  },
  slidershow: {
    flexDirection: 'row',
    marginTop: 40,
    alignItems: 'center',
  },
});
class ThreeDot extends React.PureComponent {
  render() {
    let { index } = this.props;
    return (
      <View style={styles.slidershow}>
        <View style={index == 0 ? styles.act_dot : styles.inact_dot}></View>
        <View style={index == 1 ? styles.act_dot : styles.inact_dot}></View>
        <View style={index == 2 ? styles.act_dot : styles.inact_dot}></View>
      </View>
    )
  }
}

export default ThreeDot