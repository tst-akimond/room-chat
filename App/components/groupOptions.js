import React, {Component} from 'react';
import {StyleSheet,View,Text,TextInput,Button,ActivityIndicator} from 'react-native';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';





class buttonOptions extends Component
{
    is_Mounted=true;
    state={
        groupData:[],
        loading:false,
        username:""
        
    }
   
    componentDidMount()
    {
        this.is_Mounted=true;
        database()
        .ref('/users/'+auth().currentUser.uid)
        .once('value', snapshot => {
            let uData=snapshot.val();
            if(this.is_Mounted)
            {
                this.setState({
               username:uData.username
           })
         }
        })

       database().ref("/groups").orderByChild('status').equalTo("created").on('value',snapshot=>{
           let groupData=snapshot.val();
           
           let grpData=[];
           for(let i in groupData)
           {
               if(groupData[i].private==false)
               {
                   groupData[i].key=i;
                    grpData.push(groupData[i])
           
               }
            }
        if(this.is_Mounted)
        {
           this.setState({
               groupData:grpData
           })
        
        }
       })
    }

    addtoRandomGroup=()=>{
        this.setState({
            loading:true
        });
        let grpCopy=this.state.groupData;
        let groupCount=grpCopy.length;
        if(groupCount>0)
        {
            
            let randomIndex=Math.floor(Math.random() * groupCount);
            let ranGroupData=grpCopy[randomIndex];
            let is_participation=0;

            
            let participants=ranGroupData.paritications;
            let group_id=ranGroupData.key
            for(let i in participants)
            {
                if(participants[i].uid==auth().currentUser.uid)
                {
                    is_participation=1;
                    
                    break;
                }
            }
           
      
            if(is_participation)
            {
                this.props.navigation.navigate("Prechat",{group_id:group_id})
            }
            else
            {

                this.joinGroupFromId(group_id)
            }


        }
        else
        {
            let formtoSubmit={}
            formtoSubmit['created_by']=auth().currentUser.email;
            formtoSubmit['created_id']=auth().currentUser.uid;
            formtoSubmit['private']=false;
            formtoSubmit['mode']="storymode";
            formtoSubmit['status']="created";
            formtoSubmit['notes']="";
            formtoSubmit['group']=this.randomString(16, 'abcdefghijklmnopqrstuvwxyz');
            

            const ref= database()
                .ref('/groups')
                .push(formtoSubmit);
           
            if(ref.key)
            {
                    database()
                    .ref('/groups/'+ref.key+"/paritications").push({username:this.state.username,'uid':auth().currentUser.uid}).then(()=>{
                    this.setState({
                        loading:false
                    })  
                    this.props.navigation.navigate("Prechat",{group_id:ref.key})
                })
            }
            else
            {
                this.setState({
                    loading:false
                })  
            }
        }
    }

    randomString=(length, chars)=>  {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    componentWillUnmount()
    {
        this.is_Mounted=false;
    }

    joinGroupFromId(groupid)
    {
        

        const newRef= database()
                .ref('/groups/'+groupid+'/paritications/')
                .push({
                    uid:auth().currentUser.uid,
                    username:this.state.username
                }).then(()=>{
                   this.setState({
                       loading:false
                   })
                   this.props.navigation.navigate("Prechat",{group_id:groupid})
            })
    }

    render()
    {
        if(this.state.loading)
        {   return(
            <View style={styles.loading}>
            <ActivityIndicator/>
           </View>
            )
        }
        else
    {
    return(

          <View style={{flex:1,alignItems:"center",justifyContent: 'center'}}>
             <View style={{marginBottom:10,width:200}}>
             <Button
               title="Friends"
                onPress={()=>this.props.navigation.navigate('Create')}
             />
             </View>

             <View style={{width:200}}>
             <Button
               title="Online"
                onPress={this.addtoRandomGroup}
             />
             </View>

            </View>


    )
    }
    }

}

const styles = StyleSheet.create({

    loading:{
        flex:1,
        backgroundColor:'#fff',
        alignItems:"center",
        justifyContent:"center"
    }
})

export default buttonOptions;