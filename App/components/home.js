import React,{ useEffect } from 'react';
import {StyleSheet,ScrollView,View,Text,TouchableOpacity,Button,Alert} from 'react-native';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';


class HomeComponent extends React.Component{
    
    _isMounted=false;
    state={
            groups:[],
            open_groups:[],
            user_groups:null,
            opened_groups:"",
            username:""
    }
    
    
    componentDidMount()
    {
        

        this._isMounted=true;
        if(auth().currentUser)
        {
            
           
            
            this.messageListener = messaging().onMessage(async remoteMessage => {
     
            Alert.alert(remoteMessage.notification.title+"\n"+remoteMessage.notification.body);
            this.props.navigation.navigate("Chat",{group_id:remoteMessage.data.group_id})
           
          });


        
        this.notficatonMessageListener=messaging().onNotificationOpenedApp(remoteMessage => {
            if(remoteMessage)
            {
               
            if(remoteMessage.data.type=="chart_started")
            {
                this.props.navigation.navigate("Prechat",{group_id:remoteMessage.data.group_id})
            
            }
            }
          });

        messaging()
          .getInitialNotification()
          .then(remoteMessage => {
            if(remoteMessage)
            {
                if(remoteMessage.data.type=="chart_started")
                {
                   
                    this.props.navigation.navigate("Prechat",{group_id:remoteMessage.data.group_id})
                
                }
            }    
        });

        }
  
       database()
        .ref('/users/'+auth().currentUser.uid)
        .once('value', snapshot => {
            let uData=snapshot.val();
        if(this._isMounted)
        {
           this.setState({
               username:uData.username
           })
        }
        })

        this.owngroups=database()
            .ref('/groups/').orderByChild("created_id").equalTo(auth().currentUser.uid)
            .on('value', snapshot => {
            if(!snapshot)
                return
              let userGroups=snapshot.val();
              let usrGrps=[];
              for(let i in userGroups)
                {
                    
                        userGroups[i].group_id=i
                        usrGrps.push(userGroups[i])
                    
                }
            if(this._isMounted)
            {
              this.setState({
                user_groups:usrGrps
              })
            }
        });
        
        this.open_groups=database()
        .ref('/groups/')
        .on('value', snapshot => {
            if(!snapshot)
                return
          let openGroups=snapshot.val();
        
         let openGrps=[];
         for(let i in openGroups)
         {
             if(openGroups[i].created_id!=auth().currentUser.uid)
            {   
                openGroups[i].group_id=i
                openGrps.push(openGroups[i])
            }
         }
       
        if(this._isMounted)
        {
          this.setState({
            open_groups:openGrps
          })
        }
    });
    }

    componentWillUnmount() {
        this.messageListener();
        this.notficatonMessageListener();
     
        this.owngroups();
        this.open_groups();
        this._isMounted=false;
      

    }
    
    

    getopenGroups()
    {
        
        const open_groups=this.state.open_groups;
        let GroupViews=""
        if(open_groups)
        {
            return(
                open_groups.map(item=>(
                <TouchableOpacity key={item.group_id} onPress={()=>this.props.navigation.navigate("Prechat",{group_id:item.group_id})}>
               <View  style={{width:'100%',padding:10,marginTop:10,backgroundColor:'#ccc'}}><Text  style={{textAlign:'center',fontSize:18}}>{item.group}</Text></View>
               </TouchableOpacity>
                ))
            )
               
        }
        else
        {
            return(
                <View  style={{width:'100%',padding:10,marginTop:10,backgroundColor:'#ccc'}}><Text  style={{textAlign:'center'}}>You don't have any group to Join</Text></View>
             
            )
        }
        
    }

    

    getUserGroups()
    {
        
        const user_groups=this.state.user_groups;
        let GroupViews=""
        if(user_groups)
        {
            return(
               user_groups.map(item=>(
                <TouchableOpacity key={item.group_id} onPress={()=>this.props.navigation.navigate("Prechat",{group_id:item.group_id})}>
               <View  style={{width:'100%',padding:10,marginTop:10,backgroundColor:'#ccc'}}><Text  style={{textAlign:'center',fontSize:18}}>{item.group}</Text></View>
               </TouchableOpacity>
                ))
            )
               
        }
        else
        {
            return(
                <View  style={{width:'100%',padding:10,marginTop:10,backgroundColor:'#ccc'}}><Text  style={{textAlign:'center'}}>You don't have any group</Text></View>
             
            )
        }
        
    }
    
    

    render()
    {
       

        return(
            <ScrollView>
            <View style={{flex:1}}>
               <View style={{borderBottom:"#000",borderBottomWidth:2,paddingBottom:10}}>
                   <Text style={{textAlign:"center",marginTop:10}}>User Groups</Text>
                {this.getUserGroups()}
                </View>

                <View style={{}}>
                   <Text style={{textAlign:"center",marginTop:10}}>Open Groups</Text>
                {this.getopenGroups()}
                </View>
                { <Button
                        title={"Signout"}
                        onPress={()=>{
                                    auth()
                            .signOut()
                            .then(() => console.log('User signed out!'));}}
                        /> } 
            </View>
           
            </ScrollView>
        )   
    }
}

export default HomeComponent