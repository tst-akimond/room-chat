import axios from 'axios';

export const sendPushNotification = async (tokens,title,body,payload) => {
  const FIREBASE_API_KEY = "AAAAF-dqsG8:APA91bG3tqTK__Aw1-Ul8lb6DDGTXqSzLgsmxmwLwXe_86jJtlxDxn79F_cYZN-N6XkkExWBrqKaK61zLtJ_SNhHDCU5Gp6Efk792ri4njXq39T6s5vdLhHlXV6Nkr-_tlRAfiBYInFb";
 
  const message = {
   registration_ids:tokens,
    notification: { 
      title: title,
      body: body,
      "vibrate": 1,
      "sound": 1,
      "show_in_foreground": true,
      "priority": "high",
      "content_available": true,
    },
    data:payload
}

let headers = new Headers({
    "Content-Type": "application/json",
    "Authorization": "key=" + FIREBASE_API_KEY
  });

  let response = await fetch("https://fcm.googleapis.com/fcm/send", { method: "POST", headers, body: JSON.stringify(message) })

  response = await response.json();

  return response
}
