import React, { PureComponent } from 'react';
import Proptypes from 'prop-types';
import { Text, StyleSheet, View, TouchableOpacity, ActivityIndicator,Image } from 'react-native';

export default class IconizedButton extends PureComponent {
  onButtonPress = () => {
    if(this.props.onPress)
    this.props.onPress();
  };

  render = () => {
    const borderRadius = { borderRadius: this.props.borderRadius };
    const isLoading = this.props.isLoading;
    const disabled = this.props.disabled;
    const dotted = this.props.dotted;
    const icon = this.props.icon;
    return (
      <TouchableOpacity
        style={[
          styles.buttonView,
          this.props.filled ? styles.filled : null,
          borderRadius,
          this.props.style,
          dotted? {borderStyle: 'dashed', borderWidth: 1, borderColor: '#FF7699'} : null
        ]}
        onPress={this.onButtonPress}
        disabled={disabled} 
      >
        <View style={styles.button}>
          { this.props.icon && 
            <View style={[styles.icon, {justifyContent: 'center', alignItems: 'center'}, this.props.iconStyle]}>
            <Image source={this.props.icon} style={{width:"100%", height:"100%"}} resizeMode={'contain'} /> 
            {
              this.props.badgeNumber && 
              <View style={[styles.badgeContainer, this.props.badgeStyle]}> 
                <Text style={[styles.badgeNumber, this.props.badgeNumberStyle]}>
                  {this.props.badgeNumber}
                </Text>
              </View>
            }
            </View>
          }
          

          {
            this.props.buttonTitle &&
            <Text style={[this.props.filled ? styles.filledTitle : styles.title, { fontSize: this.props.fontSize }, this.props.titleStyle]}>
              {isLoading && this.props.loadingTitle ? this.props.loadingTitle : this.props.buttonTitle}
            </Text>
          }
         
          {isLoading && (
            <View style={{ left: 8 }}>
              <ActivityIndicator color={this.props.filled ? styles.filledIndicator : 'rgba(255,255,255,0.8)'} />
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  };
}

IconizedButton.proptypes = {
  buttonTitle: Proptypes.string,
  onPress: Proptypes.func.isRequired,
  filled: Proptypes.bool.isRequired,
  borderRadius: Proptypes.number,
  iconStyle: Proptypes.object,
  badgeStyle: Proptypes.badgeStyle,
  titleStyle: Proptypes.titleStyle,
  badgeNumber: Proptypes.number
};

IconizedButton.defaultProps = {
  borderRadius: 20,
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  filled: {
    backgroundColor: '#FF769919'
  },
  title: {
    fontSize: 15,
    color: 'rgba(255, 255, 255, 0.9)',
    alignSelf: 'center'
  },
  filledTitle: {
    fontSize: 14,
    color: '#FF7699',
  },
  filledIndicator: {
    color: '#FF7699C8',
  },
  disabled: {
    opacity: 0.7,
  },
  buttonView: {
    height: 38,
    paddingVertical: 10,
    alignItems: 'center'
  },
  icon: {
    width: 20,
    height: 20,
  }, 
  badgeContainer: {
    position: 'absolute',
    backgroundColor: '#FF7699',
    justifyContent: 'center',
    alignItems: 'center',
    width: 11,
    height: 11,
    right: -5.5,
    top: -5.5,
    borderRadius: 5.5
  },
  badgeNumber: {
    color: 'white',
    fontSize: 7
  }
});
