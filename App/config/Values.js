import { Platform, Dimensions } from "react-native";

import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;

export default {
  deviceHeight,
  deviceWidth,
  platform,
  statusBarHeight: platform == 'ios' ? getStatusBarHeight(true) : 24,
  bottomSpace: platform == 'ios' ? getBottomSpace() : 0,

  //size
  fontSize: {
    xxLarge: 30,
    xLarge: 25,
    large: 20,
    medium: 16,
    small: 14,
    xSmall: 12,
    xxSmall: 11,
    xxxSmall: 10,
  },

  //styles
  roundedButton: {
    padding: 8,
    borderRadius: 5,
    elevation: 2,
    margin: 4
  }
};






